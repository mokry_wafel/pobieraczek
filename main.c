#include "libs/curl_methods.h"
#include "libs/download_methods.h"
#include "libs/output.h"

int main(int argc, char **argv) {
    output_welcome_user();
    if (argv[1]) {
        argv[1] = prepare_url_4_download(argv[1], "www.");
        get_html_page(argv[1]);
        find_urls_in_html(argv[1], argv[2]);
    } else
        output_warn();
    output_greetings();
    return 0;
}