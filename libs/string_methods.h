//
// Created by mokry_wafel on 2019-12-11.
//
#ifndef _STRING_METHODSH_
#define _STRING_METHODSH_

#include <string.h>

static const int END_OF_STRING = '\0';
static const int QUESTION_MARK = 63;
static const int DOT_MARK = 46;
static const int SPACE = ' ';
static const int DOUBLE_QUOTE = '\"';

char *concat(char *s1, char *s2) {
    char *result = malloc(strlen(s1) + strlen(s2) + 1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

char **get_urls(char *source, int size, char *pattern) {
    char **urls = malloc(size * sizeof(char *));
    for (int i = 0; i < size; ++i)
        urls[i] = malloc(1000);

    int index, flag;
    for (int element = 0; element < size; ++element) {
        source = strstr(source, pattern);
        source[0] = SPACE;
        index = 0, flag = 1;
        while (flag) {
            if (source[index + strlen(pattern)] != DOUBLE_QUOTE)
                urls[element][index] = source[index + strlen(pattern)];
            else {
                flag = 0;
                urls[element][index] = END_OF_STRING;
            }
            index++;
        }
    }
    return urls;
}

int count_urls(char *source_cpy, char *pattern) {
    int size = 0, flag = 1;
    while (flag)
        if (source_cpy && strstr(source_cpy, pattern)) {
            source_cpy[0] = SPACE;
            source_cpy = strstr(source_cpy, pattern);
            size++;
        } else
            flag = 0;
    return size;
}

char *get_extension(char *url) {
    int counter = 0;
    for (int i = strlen(url) - 1; i > 0; --i)
        if (url[i] == DOT_MARK)
            break;
        else
            counter++;
    char *extension = malloc(counter + 1);
    if (counter != strlen(url) - 1) {
        int index = 0, end_index = 0;
        for (int i = strlen(url) - counter; i < strlen(url); ++i) {
            if (url[i] == DOT_MARK || url[i] == QUESTION_MARK)
                break;
            else
                extension[index++] = url[i];
            end_index++;
        }
        extension[end_index] = END_OF_STRING;
    } else
        extension = "ERR\0";
    if (strchr(extension, '/'))
        extension = "ERR\0";
    return extension;
}

#endif