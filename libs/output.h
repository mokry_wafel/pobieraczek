//
// Created by mokry_wafel on 2020-01-08.
//

#ifndef _OUTPUT_H_
#define _OUTPUT_H_

#include <stdio.h>

void output_welcome_user(){
    printf("__________________________Pobieraczek__________________________");
}

void output_searching_tag(){
    printf("\n$ SEARCHING...");
}

void output_indexing_tag(){
    printf("\n$ Indexing...");
}

void printProgress(int qty, int iteration) {
    printf("\tProgress: %2.0f%%", ((double) (iteration + 1) / (double) qty) * 100);
}

void output_warn(){
    printf("\n You have to provide a website url!");
}
void output_greetings(){
    printf("\n____________________________Cheers!____________________________\n\n");
}

#endif