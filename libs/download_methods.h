//
// Created by mokry_wafel on 2020-01-09.
//

#ifndef _DOWNLOAD_METHODS_H_
#define _DOWNLOAD_METHODS_H_

#include <stdio.h>
#include <stdlib.h>
#include "string_methods.h"
#include "curl_methods.h"
#include "output.h"

void free_memory(char *source, char *source_cpy, int size, char **urls) {
    for (int i = 0; i < size; ++i)
        free(urls[i]);
    free(urls);
    free(source);
    free(source_cpy);
}

char *get_source() {
    FILE *file;
    file = fopen(DIRECTORY""HTML_FILE, "r");
    fseek(file, 0, SEEK_END);
    long fileSize = ftell(file);
    rewind(file);
    char *source = malloc(fileSize + 1);
    fread(source, 1, fileSize, file);
    remove(DIRECTORY""HTML_FILE);
    return source;
}

void find_urls_in_html(char *baseUrl, char *extension) {
    char *source = get_source();
    char *pattern = "src=\"";
    char *source_cpy = malloc(strlen(source) * sizeof(char));
    strcpy(source_cpy, strstr(source, pattern));

    int size = count_urls(source_cpy, pattern);
    char **urls = get_urls(source, size, pattern);

    output_searching_tag();
    save_files_by_urls(urls, baseUrl, extension, size);
    free_memory(source, source_cpy, size, urls);
}

#endif