//
// Created by mokry_wafel on 2019-12-11.
//

#ifndef _CURL_METHODSH_
#define _CURL_METHODSH_

#define DIRECTORY "downloaded/"
#define HTML_FILE "page.html"

#include <curl/curl.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <zconf.h>
#include "string_methods.h"
#include "output.h"
#include "url_utils.h"

static const int IDENTICAL = 0;

void clean_up(void *curl, FILE *file, char *filename, char *fullUrl) {
    int result = curl_easy_perform(curl);
    if (result == CURLE_OK)
        printf("\tSuccess! ... saved to:\'%s\' | URL: %s", filename, fullUrl);
    else {
        printf("\tFailure: %s | %s", fullUrl, curl_easy_strerror(result));
        remove(filename);
    }
    free(fullUrl);
    free(filename);
    fclose(file);
}

void get_html_page(const char *url) {
    CURL *curl;
    curl = curl_easy_init();
    FILE *file;
    mkdir(DIRECTORY"", S_IRWXU | S_IRWXG | S_IRWXO);
    file = fopen(concat(DIRECTORY, HTML_FILE), "w");
    fprintf(stdout, "\n$ Downloading HTML page...");
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);

    int result = curl_easy_perform(curl);
    if (result == CURLE_OK)
        printf("\n$ Success: "HTML_FILE" page saved to \""DIRECTORY"\" directory!");
    else
        printf("\n$ Failure: %s", curl_easy_strerror(result));
    fclose(file);
    curl_easy_cleanup(curl);
}

void save_files_by_urls(char **urls, char *baseUrl, char *extension, int qty) {
    CURL *curl;
    curl = curl_easy_init();
    FILE *file;
    int success_download_counter = 0;
    printf("\n$ Found %d item/s..\n$ Starting Download:", qty);
    for (int i = 0; i < qty; ++i) {
        printf("\n$ Processing... [%d/%d]", i + 1, qty);
        char *filename = malloc(20);
        char *localExtension = get_extension(urls[i]);
        if (strcmp(localExtension, "ERR") != 0) {
            if (extension == NULL || strcmp(extension, localExtension) == IDENTICAL) {
                sprintf(filename, DIRECTORY
                        "file_%d.%s%c", i + 1, localExtension, END_OF_STRING);
                printProgress(qty, i);
                if (strcmp(localExtension, "js") == IDENTICAL)
                    file = fopen(filename, "w");
                else
                    file = fopen(filename, "wb");
                char *fullUrl = prepare_url_4_download(urls[i], baseUrl);
                curl_easy_setopt(curl, CURLOPT_URL, fullUrl);
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
                curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
                curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);

                success_download_counter++;
                clean_up(curl, file, filename, fullUrl);
            } else
                printf("\tSkipping... %s", localExtension);
            free(localExtension);
        } else
            printf("\tSkipping.. : %s", urls[i]);
    }
    curl_easy_cleanup(curl);
    if (success_download_counter == 0 && extension != NULL) {
        printf("\n$ There was no files with such extension: %s", extension);
        rmdir(DIRECTORY"");
    }
}

#endif