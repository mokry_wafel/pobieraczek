//
// Created by mokry_wafel on 2020-01-09.
//

#ifndef POBIERACZEK_URL_UTILS_H_
#define POBIERACZEK_URL_UTILS_H_

int examineUrl(char *url) {
    char https[] = "https:", http[] = "http:", www[] = "www.", trick[] = "//";
    int flag = 0;
    for (int i = 0; i < strlen(url); ++i)
        if (url[i] == http[i]) {
            flag = 1;
            if (strlen(http) - 1 == i)
                break;
        } else if (url[i] == https[i]) {
            flag = 1;
            if (strlen(https) - 1 == i)
                break;
        } else if (url[i] == www[i]) {
            flag = 1;
            if (strlen(www) - 1 == i)
                break;
        } else if (url[i] == trick[i]) {
            flag = 2;
            if (strlen(trick) - 1 == i)
                break;
        } else {
            flag = 0;
            break;
        }
    return flag;
}

char *prepare_url_4_download(char *url, char *baseUrl) {
    char *resultUrl = malloc(1000);
    int flag = examineUrl(url);
    if (flag == 1)
        sprintf(resultUrl, "%s%c", url, END_OF_STRING);
    else if (flag == 2) {
        char copy_url[strlen(url)];
        strcpy(copy_url, url);
        copy_url[0] = ' ', copy_url[1] = ' ';
        sprintf(resultUrl, "%s%c", copy_url, END_OF_STRING);
    } else
        sprintf(resultUrl, "%s%s%c", baseUrl, url, END_OF_STRING);
    return resultUrl;
}

#endif